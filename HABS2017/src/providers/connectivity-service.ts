import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { Platform } from 'ionic-angular';
import { AppSettings } from './app-settings';
declare var Connection;
 
@Injectable()
export class ConnectivityProvider {
 
  onDevice: boolean;
 
  constructor(public platform: Platform, private network: Network){
    this.onDevice = this.platform.is('cordova');

    if(this.onDevice && this.network.type){
	    this.network.onConnect().subscribe(() => {
		  console.log('network connected device!');
		  AppSettings.IS_ONLINE = true;
		});
	}else if(navigator.onLine){
		console.log('network connected navigator!');
		AppSettings.IS_ONLINE = true;
	}else{
		AppSettings.IS_ONLINE = false;
	}
  }
}   