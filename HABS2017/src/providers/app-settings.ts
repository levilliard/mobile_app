
import { UserModel } from '../models/user';

export class AppSettings {
  //  public static URL_BASE = "http://192.168.1.13:8080/haccof/haccof";
	public static URL_BASE = "https://secret-island-76107.herokuapp.com/haccof";
	public static LANGUAGE = "en";
	public static IS_LOGIN =false;
	public static IS_REGISTERED =false;
	public static IS_ONLINE = false;
	public static USER_POSITION = {};
  public static DEFAULT_USER = new UserModel("1", "click", "bot", "boss", "F", "0000", "clickbot@oneclick.ht", "clickbot", "", "", "assets/img/user/user.jpg", "", "");

    public static  getCurrentDate(timezone: boolean): string{
      let dt = new Date();
      if(timezone){
        return dt.getFullYear() + "-" + ("0" + (dt.getMonth() + 1)).slice(-2) + "-" + ("0" + (dt.getDay() + 1)).slice(-2) + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds(); 
      }else{
      	return dt.getFullYear() + "-" + ("0" + (dt.getMonth() + 1)).slice(-2) + "-" + ("0" + (dt.getDay() + 1)).slice(-2); 
      }
    }
}
