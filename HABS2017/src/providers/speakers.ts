

import { Injectable } from '@angular/core';
import { SpeakersModel } from '../models/speakers';
/*
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
*/


@Injectable()
export class Speakers {

  constructor() {

  }

  getSpeakers(): Array<SpeakersModel> {
  	let speakers = new Array<SpeakersModel>();
    speakers.push(new SpeakersModel("2", "Sandra Pierre", "", "Haitian Business","", "", "assets/img/speakers/2.jpg"));
    speakers.push(new SpeakersModel("11", "Jean Baden Dubois", "", "Governer of Banque de la république d'Haiti","", "", "assets/img/speakers/11.jpg"));
    speakers.push(new SpeakersModel("9", "Beatrice Nadal Mevs", "", "Association Touristique d'Haiti(ATH)","", "", "assets/img/speakers/9.jpg"));
    speakers.push(new SpeakersModel("7", "Marc Alain Boucicault", "", "ELAN/HAITI","", "", "assets/img/speakers/7.jpg"));
    speakers.push(new SpeakersModel("5", "Colombe Emilie Jessy Menos", "", "Minister of Tourism","", "", "assets/img/speakers/5.jpg"));
    //speakers.push(new SpeakersModel("3", "Sandra Pierre", "", "Haitian Business","", "", "assets/img/speakers/3.jpg"));
    speakers.push(new SpeakersModel("4", "Dr. Pierre Marie Du Meny", "", "Minister of Commerce of Haiti","", "", "assets/img/speakers/4.jpg"));
    speakers.push(new SpeakersModel("6", "Jean Monestime", "", "Commissioner of MIAMI-DADE COUNTY","", "", "assets/img/speakers/6.jpg"));
    //speakers.push(new SpeakersModel("8", "Wanda Tima", "", "L'UNIONSUITE","", "", "assets/img/speakers/8.jpg"));
    speakers.push(new SpeakersModel("10", "Mr. Gandy Thomas", "", "COUNSEL GENERAL OF HAITI IN MIAMI","", "", "assets/img/speakers/10.jpg"));
    speakers.push(new SpeakersModel("12", "Doudly Elius", "", "Empower Haitian Women","", "", "assets/img/speakers/12.jpg"));
    speakers.push(new SpeakersModel("13", "Stephane Jean Baptiste", "", "Chief Operating Officer at Kreyol Essence, LLC","Marketing professional with 10+ years of experience in a broad range of marketing activities which includes: ocial media marketing & management, graphic design, organizational branding, market research & analysis, public relations management, media planning & buying, proposal coordination, search engine optimization and visual merchandising. Articulate communicator with strong interpersonal skills and unique ability to connect with individuals from diverse backgrounds.", "", "assets/img/speakers/13.jpg"));
    speakers.push(new SpeakersModel("14", "Wanda Tima", "", "L’union Suite","Wanda Tima-Gilles is a Haitian-Turks Islander who in 2011 launched “L’union Suite”, a Haitian-American blog dedicated to highlighting positive and uplifting stories about Haitian living abroad from celebrities to every day heroes. By December 2011, Wanda co-founded “The Haitian-American” – the Facebook page with over 100k fans and reaching 5 Million people weekly.", "", "assets/img/speakers/14.jpg"));
    speakers.push(new SpeakersModel("15", "Dominique Jean-Jacques", "", "Associate Marketing Director, NAAHP"," Graduate of Florida International University with a degree in Communications at age 20 and now 23, Dominique Jean-Jacques is a Public Relations, Marketing and Brand Strategist, freelance writer, Associate Marketing Director for the National Alliance for The Advancement of Haitian Professionals and founder of The Path Agency, a multi-platform communications firm specializing in branding, public relations, marketing, design, and events", "", "assets/img/speakers/15.jpg"));
    speakers.push(new SpeakersModel("16", "Tamara Rodriguez", "", "Children Book Author & CFO at Fatima Group","FATIMA GROUP   University of Miami - School of Business Miami/Fort Lauderdale Area ", "", "assets/img/speakers/16.jpg"));
    speakers.push(new SpeakersModel("17", "Christian Fombrun", "", "Royal Decameron","", "", "assets/img/speakers/17.jpg"));
    speakers.push(new SpeakersModel("18", "Paul Altidor", "", "Ambassador of the Embassy of the Republic of Haiti in the US","", "", "assets/img/speakers/18.jpg"));
    speakers.push(new SpeakersModel("19", "Jene Thomas", "", " Mission Director, USAID in Haiti","", "", "assets/img/speakers/19.jpg"));
    speakers.push(new SpeakersModel("20", "Frandley Julien", "", "Le National","Quality Assurance & Communication Specialist at Carnival Cruise Lines Journal Le National Florida International University - College of Law Miami/Fort Lauderdale Area", "", "assets/img/speakers/20.jpg"));
    speakers.push(new SpeakersModel("21", "Youri Latortue, Senator", "", "President of Haiti Senate","", "", "assets/img/speakers/21.jpg"));
    speakers.push(new SpeakersModel("22", "Cholzer Chancy", "", "Congressman, President of the House of Representatives of Haiti","", "", "assets/img/speakers/22.jpg"));
    speakers.push(new SpeakersModel("23", "Daniel Rouzier", "", "Chairman of the Board at Food For The Poor Haiti E-Power S.A. Dartmouth College - The Tuck School of Business at Dartmouth","", "", "assets/img/speakers/23.jpg"));
    speakers.push(new SpeakersModel("24", "Robert Paret", "", "CEO of Profin","President Directeur General at ProFin ProFin   Université de Rouen", "", "assets/img/speakers/24.jpg"));
    speakers.push(new SpeakersModel("41", "Sebastion Buteau", "", "Association Touristique d’ Haiti (ATH)","", "", "assets/img/account/user.jpg"));
    speakers.push(new SpeakersModel("25", "Francois Guillaume", "", "Board member of HACCOF","As an economist, his decade long experience in senior finance and accounting positions, led Mr. Guillaume in 2006 to open his own consulting firm GuiLac, that specializes in business/economic development and financial consulting services, where his firm has consulted and managed multi-million dollar organizations in the areas of finance, accounting, marketing and international business.", "", "assets/img/speakers/25.jpg"));
    speakers.push(new SpeakersModel("26", "Tessa Jacques", "", "General Director of Centre de Facilitation des Investissements","General Director at Centre de Facilitation des Investissements en Haïti (CFI) Centre de Facilitation des Investissements (CFI)   Palm Beach Atlantic University", "", "assets/img/speakers/26.jpg"));
    speakers.push(new SpeakersModel("27", "Harold Charles", "", "CEO of Ceepco","Commercial & Govt Construction/Facilities Management | Transforming client visions into reality on time & within budget CEEPCO Contracting, LLC.   University of Maryland College Park Washington D.C. Metro Area ", "", "assets/img/speakers/27.jpg"));
    speakers.push(new SpeakersModel("28", "Philippe Armand", "", "Founder of The Haitian-American Chamber of Commerce of Florida (HACCOF)","", "", "assets/img/speakers/28.jpg"));
    speakers.push(new SpeakersModel("29", "Michelle Austin Pamies", "", "Board Member of HACCOF","", "", "assets/img/speakers/29.jpg"));
    speakers.push(new SpeakersModel("30", "Maarten Boute", "", "Digicel","Chairman at Digicel Haiti Digicel Group, Life Haiti", "", "assets/img/speakers/30.jpg"));
    speakers.push(new SpeakersModel("31", "Stephanie Barnes", "", "Marriott Port Au Prince","Director of Sales at Marriott Port-au-Prince Marriott Hotels, University of Massachusetts Dartmouth", "", "assets/img/speakers/31.jpg"));
    speakers.push(new SpeakersModel("32", "Tamara Magloire", "", "Best Western","Marketing Manager at Best Western Premier Petion-Ville, Nova Southeastern University", "", "assets/img/speakers/32.jpg"));
    speakers.push(new SpeakersModel("34", "Rudy N. Brioché", "", "Vice Presiden & Policy Counsel at Comcast Corp","Rudy N. Brioché serves as Vice President for Global Public Policy and Policy Counsel for Comcast Corporation. In this role he focuses on the development of the company’s public policy positions and legislative analysis. Prior to joining Comcast, Rudy served as Legal Advisor to Commissioner Jonathan S. Adelstein at the Federal Communications Commission and Legislative Counsel to U.S. Senator Frank Lautenberg (D-NJ). In these roles, he focused on media and broadband policy.", "", "assets/img/speakers/34.jpg"));
    speakers.push(new SpeakersModel("33", "Pascal Desroches", "", "Executive Vice President & Chief Financial Officer at Turner","Mr. Pascal Desroches has been the Chief Financial Officer and Executive Vice President of Turner Broadcasting System, Inc., since January 01, 2015. Mr. Desroches served as Senior Vice President and Controller of Time Warner Inc. from January 1, 2008 to 2014 and also served as its Principal Accounting Officer. In this position, Mr. Desroches was responsible for overseeing a broad range of financial matters for Time Warner, including internal and external financial reporting, financial planning and analysis, as well as providing support for special projects, including mergers, acquisitions and other transactions.", "", "assets/img/speakers/33.jpg"));
    speakers.push(new SpeakersModel("41", "Linda Dorcena Forry", "", "State Senator of Massachusetts","", "", "assets/img/account/user.jpg"));
    speakers.push(new SpeakersModel("41", "Dr. G- Chesnel Pierre", "", "General Director of the Office National d’Assurance (ONA)","", "", "assets/img/account/user.jpg"));

    speakers.push(new SpeakersModel("35", "Andre Pierre", "", "Former Mayor of City of North Miami","", "", "assets/img/speakers/35.jpg"));
    speakers.push(new SpeakersModel("36", "Stephanie Auguste", "", "Minister of the Ministry of Haitians Living Abroad","", "", "assets/img/speakers/36.jpg"));
    speakers.push(new SpeakersModel("37", "Dr. Ludovic Comeau", "", "Depaul University, Chicago","", "", "assets/img/speakers/37.jpg"));
   
    speakers.push(new SpeakersModel("41", "Marc Georges", "", "President of Chambre De Commerce et d’Industrie du Nord","", "", "assets/img/account/user.jpg"));
    speakers.push(new SpeakersModel("39", "Ralph Edmond", "", "CEO of Farmatrix","", "", "assets/img/speakers/39.jpg"));
    speakers.push(new SpeakersModel("38", "Georgette Jean-Louis", "", "General Director of Banque de la Republique d’Haiti (BRH)","", "", "assets/img/speakers/38.jpg"));
    speakers.push(new SpeakersModel("40", "Jerry Cook", "", "Hanesbrands Inc.","", "", "assets/img/speakers/40.jpg"));


    speakers.push(new SpeakersModel("41", "Nathalie Naissance", "", "Yunus Kombit and Cedel Haiti","", "", "assets/img/account/user.jpg"));
    speakers.push(new SpeakersModel("41", "Allen Krause", "", " Economic/Commercial Officer, US Embassy in Haiti","", "", "assets/img/account/user.jpg"));

    return speakers;
  }

}

