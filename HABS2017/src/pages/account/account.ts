
import { Component } from '@angular/core';
import { ViewController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

import { UserModel } from '../../models/user';
import { GenericProvider } from '../../providers/generic';
import { AppSettings } from '../../providers/app-settings'; 
import { ValidationService } from '../../providers/validation-service'; 

declare var cordova: any;

@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
  providers: [GenericProvider]
})


export class AccountPage {

  private User:FormGroup;
  private lastImage: string;
  click_save = false;

  img: any;
  user: UserModel;


  constructor(
      private formBuilder: FormBuilder, 
      public viewCtrl: ViewController, 
      private gProvider: GenericProvider,
      public alertCtrl: AlertController
      ){

    this.user = AppSettings.DEFAULT_USER;
    this.User = this.formBuilder.group({
      hUserFirstname: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(30), Validators.required])],
      hUserLastname: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(30), Validators.required])],
      hUserSex: ['', Validators.compose([Validators.minLength(1), Validators.maxLength(1), Validators.required])],
      hUserTitle: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(30), Validators.required])],
      hUserPhone: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(15), Validators.required])],
      hUserEmail: ['', [Validators.required, ValidationService.emailValidator]],
      hUserUsername: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(30), Validators.required])],
      hUserPassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(100), Validators.required])],
    })
  }

  ionViewDidLoad() {
    /*this.userProv.getUser().then(data=>{
      this.user = <UserModel>data;
      console.log("user", this.user);
      if(this.user != null){
        this.showConfirm();
      }

    });
  */
  }

  dismiss(): void{
    //this.User.reset();
     this.viewCtrl.dismiss();
  }

  validateUser(){
    this.user = this.User.value;
    this.user.hUserDate = AppSettings.getCurrentDate(false).trim();
    this.user.hUserCode = "509";
    this.user.hUserImg = this.user.hUserUsername;
    this.user.hUserDetails = " .. ";

    console.log("Data to insert", this.user);

    this.gProvider.add(this.user, "/user").subscribe(data=>{
      console.log("Add user data success: ", data);
      AppSettings.DEFAULT_USER = this.user;
      AppSettings.DEFAULT_USER.hUserImg = AppSettings.URL_BASE + "/file/download/profile/" + this.user.hUserUsername;

      //update users data
      this.initUsers();
      this.showAlert("Success");
    }, error=>{
      console.log("Add user data error: ", error);
      this.showAlert("Error, try again later.")
    });

    if(this.click_save)
      this.dismiss();
  }


  private initUsers(){
     this.gProvider.initStorage("USERS", "/user/all");
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      title: 'Account details',
      message: 'Do you whant to reset your account ?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {

          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.User.reset();
            if(AppSettings.DEFAULT_USER.hUserId != null && AppSettings.DEFAULT_USER.hUserId.length > 0){
              this.gProvider.deleteObj(AppSettings.DEFAULT_USER.hUserId, "/user");
              AppSettings.DEFAULT_USER = new UserModel("1", "click", "bot", "boss", "F", "0000", "clickbot@oneclick.ht", "clickbot", "", "", "assets/img/user/user.jpg", "", "");
            }
            AppSettings.IS_REGISTERED = false;
          }
        }
      ]
    });
    confirm.present();
  }

  public setClick(){
    this.click_save = true;
  }

  getImgUrl(): string{
    return AppSettings.URL_BASE + "/file/download/profile/" + this.user.hUserUsername;
  }

  showAlert(msg){
    let alert = this.alertCtrl.create({
      title: 'OneClick Dev Team',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

}
