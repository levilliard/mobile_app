import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';

import { Evt0Page } from '../evt0/evt0';
import { Evt1Page } from '../evt1/evt1';

@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {
  tab0Root:any = Evt0Page;
  tab1Root:any = Evt1Page;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {}


  ionViewDidLoad() {
    console.log('ionViewDidLoad Schedule Page');
  }

}



