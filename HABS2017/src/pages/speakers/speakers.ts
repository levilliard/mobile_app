
import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { Speakers } from '../../providers/speakers';
import { SpeakersModel } from '../../models/speakers';
import { SpeakersDetailsPage } from '../speakers-details/speakers-details';
import { AppSettings } from '../../providers/app-settings'; 
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-speakers',
  templateUrl: 'speakers.html',
  providers: [Speakers]
})
export class SpeakersPage {

  speakers: Array<SpeakersModel>;
  other_speakers: Array<SpeakersModel>;
  is_load = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public spk: Speakers, private alertCtrl: AlertController) {

  }

  ionViewDidLoad(){
    this.speakers = this.spk.getSpeakers();
    this.other_speakers = this.speakers;
    //console.log("speakers", this.other_speakers);
    /*
    setTimeout(()=>{
      this.is_load = true;
    }, 10000);
    */
  }

  getSpeakerImg(obj: SpeakersModel): string{
    if(obj && obj != undefined){
      return obj.SPEAKER_IMG1;
    }
  }

  showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'Login',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  goDetailsPage(speaker){
      this.navCtrl.push(SpeakersDetailsPage, {speaker: speaker}, {animate: true, direction: 'forward'});
  }

  getItems(ev: any) {
    let val = ev.target.value;
    //this.speakers = this.spk.getSpeakers();
    console.log("event: ", val);
    // if the value is an empty string don't filter the items
    let list = [];
    if (val && val.trim() != '') {
      for(let i = 0; i < this.speakers.length; ++i){
         let str = this.speakers[i].SPEAKER_FIRSTNAME.trim();
         if(str.trim().toLowerCase().indexOf(val.toLowerCase()) > -1){
            list.push(this.speakers[i]);
         }
       }
       console.log("list", list);
      }else{
        this.other_speakers = this.speakers;
      }

      if(this.other_speakers.length != list.length && list.length > 0){
        this.other_speakers = list;
      }
   }
}
