import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { UserModel } from '../../models/user';
import { AppSettings } from '../../providers/app-settings';
import { MessagePage } from '../message/message';
import { UserDetailsPage } from '../user-details/user-details';

@Component({
  selector: 'page-user-list',
  templateUrl: 'user-list.html',
})
export class UserListPage {
  users: Array<UserModel>;
  other_users = [];
  login = AppSettings.IS_LOGIN;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController) {
  	this.users = <Array<UserModel>>this.navParams.get("users");
  	this.other_users = this.users;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  getUserImg(user: UserModel){
  	return AppSettings.URL_BASE + "/file/download/profile/" + user.hUserUsername; 
  }

  goChat(user: UserModel){
  	if(AppSettings.IS_LOGIN){
        if(AppSettings.DEFAULT_USER.hUserId == user.hUserId){
        	this.showAlert()
        }else{
			    this.navCtrl.push(MessagePage, {users: this.users, user_chat: user});
        }
      }else{
        this.showLogin();
      }
  }


  goUserDetails(user: UserModel){
    if(user){
      user.hUserImg = AppSettings.URL_BASE + '/file/download/profile/' +  user.hUserUsername;
      this.navCtrl.push(UserDetailsPage, {user: user});
    }
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Hey !',
      subTitle: "You can't send message to your self",
      buttons: ['OK']
    });
    alert.present();
  }

  showLogin() {
    let alert = this.alertCtrl.create({
      title: 'Hey !',
      subTitle: "You are not login or register !",
      buttons: ['OK']
    });
    alert.present();
  }
  getItems(ev: any) {
    let val = ev.target.value;
    //this.speakers = this.spk.getSpeakers();
    console.log("event: ", val);
    // if the value is an empty string don't filter the items
    let list = [];
    if (val && val.trim() != '') {
      for(let i = 0; i < this.users.length; ++i){
         let str = this.users[i].hUserUsername + this.users[i].hUserFirstname + this.users[i].hUserLastname;
         if(str.trim().toLowerCase().indexOf(val.toLowerCase()) > -1){
            list.push(this.users[i]);
         }
       }
       console.log("list", list);
      }else{
        this.other_users = this.users;
      }

      if(this.other_users.length != list.length && list.length > 0){
        this.other_users = list;
      }
   }
}
