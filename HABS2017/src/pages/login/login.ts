import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, ViewController, MenuController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { UserModel } from '../../models/user';
import { AppSettings } from '../../providers//app-settings';
import { GenericProvider } from '../../providers/generic'; 

import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  V
  private User:FormGroup;
  @ViewChild(Nav) nav: Nav;
  user: UserModel;
  click_save = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public formBuilder: FormBuilder, 
    public viewCtrl: ViewController,
    private gProvider: GenericProvider,
    private alertCtrl: AlertController,
    private storage: Storage
    ) {
    this.user = AppSettings.DEFAULT_USER;
    this.User = this.formBuilder.group({
      hUserUsername: ['', Validators.required],
      hUserPassword: ['', Validators.required],
    })
  }

  ionViewDidLoad() {

  }

  dismiss(): void{
     this.User.reset();
     this.viewCtrl.dismiss();
  }
/*
  private initUsers(){
     this.gProvider.initStorage("USERS", "/user/all");
  }
*/
  validateLogin(){
    let username = <string>this.User.value.hUserUsername;
    let password = <string>this.User.value.hUserPassword;

    this.storage.get("USERS").then(data=>{
     let users = <Array<UserModel>>data;

      if(users.length > 0){
        for(let i = 0; i < users.length; ++i){
            let user = users[i];
            if(username && password){
              if(username.trim().toLowerCase() === user.hUserUsername.trim().toLowerCase() && password.trim() === user.hUserPassword.trim()){
                  AppSettings.IS_LOGIN = true;
                  AppSettings.DEFAULT_USER = user;
                  AppSettings.DEFAULT_USER.hUserImg = AppSettings.URL_BASE + "/file/downloa/profile/" + user.hUserUsername; 
                 // this.initUsers();
                  this.showAlert("Login Success");
              }
          }
        }

        if(!AppSettings.IS_LOGIN){
           this.showAlert("Login Failed !");
        }
      }
    });

    if(this.click_save)
      this.dismiss();
    //this.goHome();
  }

  private showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: '',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  public goHome(){
    this.nav.setRoot(HomePage);
  }

  public setClick(){
    this.click_save = true;
  }
}
