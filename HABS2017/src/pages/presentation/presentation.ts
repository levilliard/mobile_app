import { Component, ViewChild } from '@angular/core';
import { NavController, ModalController, NavParams, AlertController } from 'ionic-angular';
import { AccountPage } from '../account/account';

import { UserModel } from '../../models/user';
import { AppSettings } from '../../providers/app-settings'; 
import { LoginPage } from '../login/login';


@Component({
  selector: 'page-presentation',
  templateUrl: 'presentation.html'
})
export class PresentationPage {
  login: boolean;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public modalCtrl: ModalController, 
    private alertCtrl: AlertController
    ){
    this.login = AppSettings.IS_REGISTERED;
  }

  ionViewDidLoad() {

  }



  presentLoginModal() {
    let modal = this.modalCtrl.create(AccountPage);
    modal.present();
  }

  showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'Login',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  goDetailsPage(){

  }
}