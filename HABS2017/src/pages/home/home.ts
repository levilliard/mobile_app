import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';

import { PresentationPage } from '../presentation/presentation';
import { MediaPage } from '../media/media';
import { SponsorsPage } from '../sponsors/sponsors';
import { MapPage } from '../map/map';
import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  tab1Root:any = PresentationPage;
  tab2Root:any = MediaPage;
  tab3Root:any = MapPage;
  tab4Root:any = SponsorsPage;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {}


  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  public openSettings(){
    let modal = this.modalCtrl.create(SettingsPage);
    modal.present();  }
}



