import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SpeakersModel } from '../../models/speakers';

@Component({
  selector: 'page-speakers-details',
  templateUrl: 'speakers-details.html'
})
export class SpeakersDetailsPage {

  speaker: SpeakersModel;
  isLoad = false;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
  	    this.speaker = <SpeakersModel>this.navParams.get('speaker');
   	  	this.isLoad = true;
  }

}
