import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { GenericProvider } from '../../providers/generic';
import { MessageModel } from '../../models/message';
import { UserModel } from '../../models/user';
import { AppSettings } from '../../providers/app-settings'
import { UserListPage } from '../user-list/user-list';
import { Observable } from 'rxjs/Rx';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {
  @ViewChild(Content) content: Content;
  messages = [];
  other_messages = [];
  user: UserModel = AppSettings.DEFAULT_USER;
  login = AppSettings.IS_LOGIN;
  userTyping = "";
  users = [];
  user_chat: UserModel;
  is_same_user = false;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams, 
  	private gProvider: GenericProvider, 
  	private storage: Storage){
      this.users = <Array<UserModel>>this.navParams.get("users");
      this.user_chat = <UserModel>this.navParams.get("user_chat");
  	  this.getMsg();
      //this.update();
  }

  ionViewDidLoad() {
    this.update();
  }

  private getMsg(){
    if(this.login){
  	  this.storage.get("MESSAGES").then(data=>{
      let msgs = <Array<MessageModel>>data;
      if(msgs.length > 0){
          for(let i = 0; i < msgs.length; ++i){
            if(msgs[i].messageIdUserReceiver.trim() == this.user.hUserId.trim()){
               for(let j = 0; j < this.users.length; ++j){
                   if(msgs[i].messageIdUserSender.trim() == this.users[j].hUserId){
                   let msg = {"data": msgs[i], "user": this.users[j].hUserUsername, "img": AppSettings.URL_BASE + "/file/download/profile/" + this.users[j].hUserUsername, "sendTo": ""};
                   this.messages.push(msg);
                 }
             }
            }else if(msgs[i].messageIdUserSender.trim() == this.user.hUserId.trim()){
                for(let j = 0; j < this.users.length; ++j){
                  if(msgs[i].messageIdUserReceiver.trim() == this.users[j].hUserId){
                    let msg = {"data": msgs[i], "user": "You", "img": AppSettings.URL_BASE + "/file/download/profile/" + this.user.hUserUsername.trim(), "sendTo": this.users[j].hUserUsername};
                    this.messages.push(msg);
                  }
                }
            }

          } 
          //a1.length==a2.length && a1.every(function(v,i) { return v === a2[i]})
          if(this.messages.length != this.other_messages.length){
            this.other_messages = this.messages;
          }
       }
     });
  	}
  }

 addMsg(){
   if(this.userTyping.trim().length > 1){
     let msg: MessageModel = new MessageModel("1", this.user.hUserId, this.user_chat.hUserId, this.userTyping, AppSettings.getCurrentDate(true));
     this.messages.push({"data": msg, "user": "You", "img": this.user.hUserUsername, "sendTo": "Unknow"});
     this.gProvider.add(msg, "/message").subscribe(data=>{
     }, error=>{
       console.log("error send msg network",  error);
     });
     this.userTyping = "";
   }else{
     let m: MessageModel = new MessageModel("1", "0", "0", "Hey ! Please type something...", AppSettings.getCurrentDate(true));
     let msg = {"data":m, "user": "ClickBot", "img":"assets/img/user/clickbot.jpg", "sendTo": "Unknow"};
     this.messages.push(msg);
     this.userTyping = "";
   }
   this.content.scrollToBottom();
   //this.update();
 }

 addUser(){
   this.navCtrl.push(UserListPage, {users: this.users});
 }

 private toRun(){
   this.messages = [];
    this.gProvider.initStorage("MESSAGES", "/message/all");
    this.gProvider.initStorage("USERS", "/user/all");
    this.getMsg();
    console.log("update...");
 }

 update(){
  let timer = Observable.timer(1000, 1500);
  timer.subscribe(t=>{
    this.toRun();
  });
 }

 /*
 bsMsgAnalyse(): MessageModel{
  if(this.userTyping.length <= 0){
     let text = " Hey ! You don't ask me nothing";
     let msg: MessageModel = new MessageModel("1", "clickbot", "assets/img/account/clickbot.jpg", text, AppSettings.getCurrentDate(true) + "");
     return msg;
   }else if(this.talkShit, this.userTyping){
      let text = " Hey ! Be wise, if you contunue to talk ";
      let msg: MessageModel = new MessageModel("1", "clickbot", "assets/img/account/clickbot.jpg", text, AppSettings.getCurrentDate(true) + "");
      return msg;
   }
  
 }
 */
}
