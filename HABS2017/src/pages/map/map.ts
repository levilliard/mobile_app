
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { AppSettings } from '../../providers/app-settings';

declare var google;
 
@Component({
  selector: 'map-page',
  templateUrl: 'map.html',
  providers: [AppSettings]
})
export class MapPage {
 
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  destination = {"ALTD":25.756788 , "LONG":-80.374356};

  constructor(public navCtrl: NavController, public alertCtrl: AlertController){

  }

  ionViewDidLoad(){
    /*setTimeout(()=>{
      if(AppSettings.IS_ONLINE){
        this.loadMap();
      }else{
         this.showAlert();
      }
    }, 2000);
    */
    this.loadMap();
  }
 
  loadMap(){
    if(typeof google != "undefined" && typeof google.maps != "undefined"){
       let latLng = new google.maps.LatLng(this.destination.ALTD, this.destination.LONG);
       let mapOptions = {
         center: latLng,
         zoom: 15,
         styles:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#0b434f"},{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#146474"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}],
         mapTypeId: google.maps.MapTypeId.ROADMAP
       }
    
       this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
   }else{
     console.log("Goople Map npt load");
   }    
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Network Connection',
      subTitle: 'You must connect to internet to load the map page. Please try again later',
      buttons: ['OK']
    });
    alert.present();
  }
}