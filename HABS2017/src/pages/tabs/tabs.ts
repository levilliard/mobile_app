import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { EventsPage } from '../events/events';
import { AboutPage } from '../about/about';
import { SpeakersPage } from '../speakers/speakers';
//import { MessagePage } from '../message/message';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  HomeRoot: any = HomePage;
  //CommunitRoot: any = MessagePage;
  SpeakersRoot: any = SpeakersPage;
  EventsRoot: any = EventsPage;
  AboutRoot: any = AboutPage;
  mySelectedIndex: number;

  constructor(navParams: NavParams) {
      this.mySelectedIndex = navParams.get("tabIndex");
  }
}
