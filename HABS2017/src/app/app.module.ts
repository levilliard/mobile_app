
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { AboutPage } from '../pages/about/about';
import { SpeakersPage } from '../pages/speakers/speakers';
import { EventsPage } from '../pages/events/events';
import { TabsPage } from '../pages/tabs/tabs';
import { SpeakersDetailsPage } from '../pages/speakers-details/speakers-details';
import { MapPage } from '../pages/map/map';
import { MessagePage } from '../pages/message/message';
import { UserListPage } from '../pages/user-list/user-list';
import { UserDetailsPage } from '../pages/user-details/user-details';
import { PresentationPage } from '../pages/presentation/presentation';
import { Evt0Page } from '../pages/evt0/evt0';
import { Evt1Page } from '../pages/evt1/evt1';
import { MediaPage } from '../pages/media/media';
import { SponsorsPage } from '../pages/sponsors/sponsors';
import { SettingsPage } from '../pages/settings/settings';
import { IonicStorageModule } from '@ionic/storage';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { ConnectivityProvider } from '../providers/connectivity-service';
import { Network } from '@ionic-native/network';
import { GenericProvider } from '../providers/generic';
import { ValidationService } from '../providers/validation-service';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';

import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '661c9db1'
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AccountPage,
    AboutPage,
    SpeakersPage,
    TabsPage,
    SpeakersDetailsPage,
    LoginPage,
    EventsPage,
    PresentationPage,
    MediaPage,
    SponsorsPage,
    MapPage,
    SettingsPage,
    MessagePage,
    UserListPage,
    UserDetailsPage,
    Evt0Page,
    Evt1Page
  ],
  imports: [
    IonicModule.forRoot(MyApp, {tabsHideOnSubPages:"true"}),
    IonicStorageModule.forRoot(),
    HttpModule,
    BrowserModule,
    IonicImageViewerModule,
    CloudModule.forRoot(cloudSettings),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }}),    
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AccountPage,
    AboutPage,
    SpeakersPage,
    TabsPage,
    SpeakersDetailsPage,
    LoginPage,
    EventsPage,
    PresentationPage,
    MediaPage,
    SponsorsPage,
    MapPage,
    SettingsPage,
    MessagePage,
    UserListPage,
    UserDetailsPage,
    Evt0Page,
    Evt1Page
    ],
  providers:[{provide: ErrorHandler, useClass: IonicErrorHandler}, 
    ValidationService, 
    ConnectivityProvider, 
    Network, 
    GenericProvider, 
    Geolocation,
    File,
    Transfer,
    FilePath, 
    Camera ]
})
export class AppModule {}
