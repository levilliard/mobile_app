import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, ModalController, AlertController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AccountPage } from '../pages/account/account';
import { SpeakersPage } from '../pages/speakers/speakers';
import { EventsPage } from '../pages/events/events';
import { TabsPage } from '../pages/tabs/tabs';
import { MessagePage } from '../pages/message/message';
import { UserListPage } from '../pages/user-list/user-list';
import { LoginPage } from '../pages/login/login';
import { GenericProvider } from '../providers/generic';
import { AppSettings } from '../providers/app-settings';
import { UserModel } from '../models/user'; 
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { UserDetailsPage } from '../pages/user-details/user-details';
import {Deploy} from '@ionic/cloud-angular';

@Component({
  templateUrl: 'app.html',
  providers: [SplashScreen, StatusBar]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = TabsPage;
  user: UserModel;
  pages: Array<{title: string, component: any}>;
  users = [];

  constructor(
    public platform: Platform, 
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public menu: MenuController, 
    public modalCtrl: ModalController, 
    public alertCtrl: AlertController,
    public gProvider: GenericProvider,
    public translate: TranslateService,
    private storage: Storage, 
    public deploy: Deploy) {
    this.initializeApp();
    this.translate.setDefaultLang('en');
    AppSettings.LANGUAGE = "en";
    this.user = AppSettings.DEFAULT_USER;

    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Account', component: AccountPage },
      { title: 'Speakers', component: SpeakersPage },
      { title: 'Events', component: EventsPage },      
      { title: '', component: TabsPage }
    ];
    //this.showAlert();
     this.intUsers();  
     this.initUserMsg();
     this.getUsers();
   }

  ionViewDidLoad(){
  }

  private intUsers(){
     this.gProvider.initStorage("USERS", "/user/all");
  }

/*
  isLogin(){
    this.storage.ready().then(() => {
      this.storage.set("IS_LOGIN", false);
    });
  }
*/
  private initUserMsg(){
     this.gProvider.initStorage("MESSAGES", "/message/all");
  }

  private getUsers(){
    this.storage.get("USERS").then(data=>{
      this.users = <Array<UserModel>>data;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }


  openPage(index) {
    this.menu.close();
    this.nav.setRoot(TabsPage, {tabIndex: index});
  }

  openMsgPage() {    
    this.menu.close();

    if(AppSettings.IS_LOGIN){
      this.nav.setRoot(MessagePage, {users: this.users});
    }else{
      this.showAlert("User Details", "Please login or register");
    }
  }

  openUserPage(){
    this.menu.close();

    //if(AppSettings.IS_LOGIN){
    //this.getUsers();
    //}else{
    //   this.showAlert("User Details", "Please login or register");
   //}
    this.storage.get("USERS").then(data=>{
      let users = <Array<UserModel>>data;
       this.nav.setRoot(UserListPage, {users: users});
    });
  }

  presentModal() {
    let modal = this.modalCtrl.create(AccountPage);
    modal.present();
  }

  presentUserDatailsPage(){
    AppSettings.DEFAULT_USER.hUserImg = AppSettings.URL_BASE + '/file/download/profile/' +  AppSettings.DEFAULT_USER.hUserUsername;
    this.menu.close();
    this.nav.setRoot(UserDetailsPage, {user: AppSettings.DEFAULT_USER});
  }

  presentLoginModal() {
    let modal = this.modalCtrl.create(LoginPage);
    modal.present();
  }

  showAlert(title: string, msg: string){
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
}
