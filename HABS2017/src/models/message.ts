
export class MessageModel{
	messageId: string;
    messageIdUserSender: string;
    messageIdUserReceiver: string;
    messageContent: string;
    messageDate: string;

    constructor(id: string, sId: string, rId: string, content: string, mDate: string){
    	this.messageId = id;
    	this.messageIdUserSender = sId;
    	this.messageIdUserReceiver = rId;
    	this.messageContent = content;
    	this.messageDate = mDate;
    }
}