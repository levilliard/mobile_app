export class SpeakersModel{
	SPEAKER_ID: string;
	SPEAKER_FIRSTNAME: string;
	SPEAKER_LASTNAME: string;
	SPEAKER_TITLE: string;
	SPEAKER_DETAILS: string;
	SPEAKER_PANEL: string;
	SPEAKER_IMG1: string;


	constructor(Id:string, fName: string, lName: string, title: string, details: string, panel: string, Img1: string){
		this.SPEAKER_ID = Id;
		this.SPEAKER_FIRSTNAME = fName;
		this.SPEAKER_LASTNAME = lName;
		this.SPEAKER_TITLE = title;
		this.SPEAKER_DETAILS = details;
		this.SPEAKER_PANEL = panel;
		this.SPEAKER_IMG1 = Img1;
	}
}