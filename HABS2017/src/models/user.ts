
export class UserModel{
    hUserId: string;
    hUserFirstname: string;
    hUserLastname: string;
    hUserTitle: string;
    hUserSex: string;
    hUserPhone: string;
    hUserEmail: string;
    hUserUsername: string;
    hUserPassword: string;
    hUserCode: string; 
    hUserImg: string;
    hUserDetails: string;
    hUserDate: string;
    isLogin: boolean;
    isSignin: boolean;

  constructor(id:string, fname: string, lname: string, title: string, sex: string, phone: string, email: string, username: string, pwd: string, code:string, img:string, details: string, hDate: string){
     this.hUserId = id;
     this.hUserFirstname = fname;
     this.hUserLastname = lname;
     this.hUserTitle = title;
     this.hUserSex = sex;
     this.hUserPhone = phone;
     this.hUserEmail = email;
     this.hUserUsername = username;
     this.hUserPassword = pwd;
     this.hUserCode = code;
     this.hUserImg = img;
     this.hUserDetails = details;
     this.isLogin = false;
     this.isSignin = false;
  }
}