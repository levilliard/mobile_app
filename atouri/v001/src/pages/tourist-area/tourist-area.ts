import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, LoadingController } from 'ionic-angular';
import { GenericProvider } from '../../providers/generic';
import { TouristAreaModel } from '../../models/tourist-area';
import { AppSettings } from '../../providers/app-settings';
import { DetailsTabPage } from '../details-tab/details-tab';

@Component({
  selector: 'page-tourist-area',
  templateUrl: 'tourist-area.html',
})
export class TouristAreaPage {
  @ViewChild(Content) content: Content;

  areas: Array<TouristAreaModel>;
  other_areas: Array<TouristAreaModel>;
  intvl = 0;

  constructor(
  	public navCtrl: NavController, 
  	private gProvider: GenericProvider,
    private loading: LoadingController) {
    this.areas = [];
    this.other_areas = [];
    this.init();

    AppSettings.URI_BASE = "/tourist";

    this.gProvider.getData("/tourist/all/tourist").subscribe(data=>{
        let temp = <Array<TouristAreaModel>>data;
        let i = temp.length;
        let j = this.areas.length;

        while(i--){
          this.areas[j+i-1] = temp[i];
        }  

         this.other_areas = this.areas;
      }, error=>{
      });

    this.presentLoading(" ");
  }



  presentLoading(param){
    let load = this.loading.create({content: param});

    load.present();

    setTimeout(()=>{
      load.dismiss();
    }, 2500);
  }

  openPageAreaDetails(area: TouristAreaModel) {
    this.navCtrl.push(DetailsTabPage, {area: area});
  }

  getIMGUri(data): string{
    if(data.indexOf("assets") != -1){
      return data + ".jpg";
    }

  	return  AppSettings.URL_BASE + "/file/download"+ AppSettings.URI_BASE + "/" + data + ".jpg";    
  }

  getItems(ev: any) {
    let val = ev.target.value;

    let list = [];
    if (val && val.trim() != '') {
      for(let i = 0; i < this.areas.length; ++i){
         let str = this.areas[i].touristAreaName.trim();
         if(str.trim().toLowerCase().indexOf(val.toLowerCase()) > -1){
            list.push(this.areas[i]);
         }
       }
      }else{
        this.other_areas = this.areas;
      }

      if(this.other_areas.length != list.length && list.length > 0){
        this.other_areas = list;
      }

      if(list.length == 0){
        this.other_areas = this.areas;
      }
   }

   moveUp(){
     this.content.scrollToTop();
   }

   private init(): void{
   let ofl: Array<TouristAreaModel> = [{ 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "Parc Pic Macaya",
        "touristAreaDesc": "Le Parc National Naturel Macaya (PNNM) créé par décret du gouvernement de Jean-Claude Duvalier en 1984, sur une superficie de 2,000 ha, augmenté à plus de 8,000 ha en mars 2013, par le Président Martelly, abrite aujourd’hui la dernière forêt primaire d’Haïti et constitue le dernier sanctuaire de biodiversité de la partie Ouest de l’île, possédant la plus grande concentration au monde d’espèces endémiques par unité de surface.",
        "touristAreaImg": "assets/img/data/ppicmacaya",
        "touristAreaDetails": "Le parc constitue la zone cœur de la réserve de biosphère de La Hotte, désignée par l'UNESCO en 2016. Le parc englobe deux mornes, le morne Formond et le morne Macaya. Ces mornes sont coupés de profondes ravines qui donnent naissance aux grandes rivières du sud de la péninsule haïtienne.",
        "touristAreaLoc": "Cayes Corail",
        "touristAreaLat": "18.33",
        "touristAreaLong": "-74.029999",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "site naturel",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    },

    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "Labadie",
        "touristAreaDesc": "Classé parmi les plus beaux villages d’Haïti, Labadie est connue comme la plus belle et la plus exclusive station balnéaire de la mer des Caraïbes. Ce village de pêche, où s’accoste régulièrement le bateau de croisière Royal Caribbean Cruise Line est une plage privée, située à 5 km au nord-ouest du Cap-Haitien. Labadie n’est accessible que par bateau de croisière. Non loin de ce site paradisiaque, il existe d’autres hôtels et plages à visiter, accessibles par la route en particulier le somptueux hôtel Cormier qui propose un service d’hôtellerie avec lequel riment confort et prix.",
        "touristAreaImg": "assets/img/data/labadie",
        "touristAreaDetails": "",
        "touristAreaLoc": "Cap-Haitien",
        "touristAreaLat": "19.772227",
        "touristAreaLong": "-72.247236",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "lieu touristique",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    },
    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "Cote des Arcadins",
        "touristAreaDesc": "La Côte des Arcadins est une longue étendue de plages de sable blanc située à seulement 45 minutes au nord de Port-au-Prince via la Route Nationale #1. On y trouve une plage publique pour les visiteurs qui veulent une aventure au bord de la mer à un tarif moindre. Pour quelques dollars de plus, vous pouvez accéder à une enfilade d’hôtels privés et de stations balnéaires qui offrent des services tels des toilettes, une cuisine créole raffinée, une piscine d’eau douce. Du poisson et des fruits de mer fraîchement pê- chés sont disponibles le long des plages de la Côte des Arcadins. Vous pouvez en acheter directement des pêcheurs et surtout n’oubliez pas d’essayer la sauce pimentée au citron (Pikliz) avec votre plat de lambi grillé. Sur la Côte des Arcadins la mer est le plus souvent tranquille et transparente, donc favorable au snorkeling et aux sports nautiques. Il est important de noter que généralement plages et piscines ne sont pas surveillées par un maître-nageur.",
        "touristAreaImg": "assets/img/data/cotesdesaccadins",
        "touristAreaDetails": "La Côte des Arcadins est la plus active des côtes maritimes, un rendez-vous incontourn- able pour des touristes qui recherchent le soleil.",
        "touristAreaLoc": "Montruis",
        "touristAreaLat": "18.963536",
        "touristAreaLong": "-72.724991",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "lieu touristique",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    },
    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "Jacmel",
        "touristAreaDesc": "Fondée en 1868 par des colons français, réputée pour ses plages, ses maisons col- orées, son artisanat et son carnaval annuel, Jacmel est le lieu d’origine de plusieurs pein- tres et poètes haïtiens célèbres. Elle est considérée comme la capitale culturelle d’Haïti. Les balcons en fer forgé qui longent les façades de ses maisons sont typiques et remon- tent à l’époque coloniale. Cette ville offre des vues saisissantes et une ambiance sans pareil durant le Carnaval de Jacmel qui est reconnue en Haïti et dans la région caribéenne.",
        "touristAreaImg": "assets/img/data/jacmel",
        "touristAreaDetails": "A la sorti de la ville, vers Marigot, un inestimable trésor historique : Le Moulin Price, un des deux spécimen de ce type de moulin à vapeur répertorié dans le monde. Des sites his- toriques s’échelonnent un peu partout dans la région Jacmellienne : Etant Bosier, Cascade Pichon, Orangers, Bermudes, Grandin, Grottes de Sejourné, Lavanneau, Meyeret le site des bassins Bleus au Nord de Jacmel.",
        "touristAreaLoc": "Jacmel",
        "touristAreaLat": "18.240423",
        "touristAreaLong": "-72.518691",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "lieu touristique",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    },

    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "Île-à-Vache",
        "touristAreaDesc": "Île-à-Vache est une île de 128 km2 de superficie qui se baigne dans l’eau claire de la mer des Caraïbes, à seulement 5,50 miles nautiques de la ville côtière des Cayes. De Port-au-Prince, la ville des Cayes est facilement accessible en voiture et les vols charter arrivent à l’aéroport régional. A partir du port des Cayes, la traversée dure 20 minutes en bateau. L’Histoire de l’Ile à Vache raconte qu’elle fut un refuge pour les pirates et bouca- niers d’où elle a acquis le surnom “L’île au Trésor.” Si vous souhaitez visiter Île-à-Vache, il est important de planifier à l’avance car Il n’existe actuellement que deux hôtels privés sur l’île. Île-à-Vache est un réel paradis. Vous croirez être sur une île déserte accueilli par une communauté de gens chaleureux et une cuisine très raffinée créole et internationale. Ne manquez pas les couchers de soleil inoubliables à partir de n’importe quel coin de l’île.",
        "touristAreaImg": "assets/img/data/ileavache",
        "touristAreaDetails": "",
        "touristAreaLoc": "Île-à-Vache",
        "touristAreaLat": "18.070598",
        "touristAreaLong": "-73.635859",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "lieu touristique",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    }];

    let i = ofl.length;
    while(i--){
      this.areas.push(ofl[i]);
    }

    this.other_areas = this.areas;
   }
}
