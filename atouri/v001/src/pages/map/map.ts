
import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';

import { AppSettings } from '../../providers/app-settings';

import { Position } from '../../models/position';

declare var google;
 
@Component({
  selector: 'map-page',
  templateUrl: 'map.html'
})
export class MapPage {
 
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  position_org = {};
  itineraire_org: any;
  itineraire_dest: any;
  destination: Position;

  constructor(private params: NavParams, private alertCtrl: AlertController) {
    this.destination = <Position>params.get("destination");
    console.log("destination: ", this.destination);
  }
 
  ionViewDidLoad(){
     console.log("map loading...");
    this.loadMap();
  }


  private showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'KasikRevo',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  loadMap(){
    var markerArray = [];
    let position: Position = AppSettings.USER_GEOLOCATION;
    if(position == null){
      console.log("position null");
      this.showAlert("Connect to internet and activate Localization on your devices");
      return ;
    }


    if(typeof google != "undefined" && typeof google.maps != "undefined"){
       this.itineraire_dest= new google.maps.LatLng(this.destination.lat, this.destination.lon);
       this.itineraire_org = new google.maps.LatLng(position.lat, position.lon);       
       let mapOptions = {
         center: this.itineraire_org,
         zoom: 15,
         fullscreenControl: false,
         styles: [{"featureType":"all","elementType":"geometry","stylers":[{"color":"#272b33"}]},{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#13ed9a"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#1a3646"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#003650"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#003650"}]},{"featureType":"administrative.neighborhood","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#13ed9a"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#000000"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#003650"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#003650"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#6f9ba5"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#003650"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#13ed9a"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#304a7d"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#2c6675"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#003650"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#13ed9a"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"color":"#003650"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"color":"#283d6a"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#003650"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#0e1626"}]},{"featureType":"water","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#4e6d70"}]}],
         mapTypeId: google.maps.MapTypeId.ROADMAP
       }
    
       this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        var directionsService = new google.maps.DirectionsService;

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        var directionsDisplay = new google.maps.DirectionsRenderer({map: this.map});

        var stepDisplay = new google.maps.InfoWindow;

        this.calculateAndDisplayRoute(directionsDisplay, directionsService, markerArray, stepDisplay, this.map);
      

   }else{
     console.log("Goople Map not load");
   }    
  }

  addMarker(){
    let marker = new google.maps.Marker({
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.position_org
    });
   
    let content = "<h4>Information!</h4>";          
   
    this.addInfoWindow(marker, content);
  }

  addInfoWindow(marker, content){
 
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
   
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  }


      showSteps(directionResult, markerArray, stepDisplay, map) {
        // For each step, place a marker, and add the text to the marker's infowindow.
        // Also attach the marker to an array so we can keep track of it and remove it
        // when calculating new routes.
        var myRoute = directionResult.routes[0].legs[0];
        for (var i = 0; i < myRoute.steps.length; i++) {
          var marker = markerArray[i] = markerArray[i] || new google.maps.Marker;
          marker.setMap(map);
          marker.setPosition(myRoute.steps[i].start_location);
          this.attachInstructionText(
              stepDisplay, marker, myRoute.steps[i].instructions, map);
        }
      }

     calculateAndDisplayRoute(directionsDisplay, directionsService,
          markerArray, stepDisplay, map) {
        // First, remove any existing markers from the map.
        for (var i = 0; i < markerArray.length; i++) {
          markerArray[i].setMap(null);
        }

        // Retrieve the start and end locations and create a DirectionsRequest using
        // WALKING directions.
        directionsService.route({
          origin: this.itineraire_org,
          destination: this.itineraire_dest,
          travelMode: 'WALKING'
        }, function(response, status) {
          // Route the directions and pass the response to a function to create
          // markers for each step.
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
            //this.showSteps(response, markerArray, stepDisplay, map);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }

      attachInstructionText(stepDisplay, marker, text, map) {
        google.maps.event.addListener(marker, 'click', function() {
          // Open an info window when the marker is clicked on, containing the text
          // of the step.
          stepDisplay.setContent(text);
          stepDisplay.open(map, marker);
        });
      }
}
