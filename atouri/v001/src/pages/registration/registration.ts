import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';

import { AccountPage } from '../account/account';
import { LoginPage } from '../login/login';
import { AppSettings } from '../../providers/app-settings';

@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {

  login = "Login for sharing with us";
  isLogin = AppSettings.IS_LOGIN;
  isRegistered = AppSettings.IS_REGISTERED;

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) {

  }

  public ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage login', this.isLogin);

    if(this.isLogin){
      this.login = AppSettings.DEFAULT_USER.hUserUsername;
    }
  }

  public goPage(page: number){
  	switch (page) {
  		case 0:
  			this.openAccontPage();
  			break;
  		
  		default:
  			this.openLoginPage();
  			break;
  	}

    console.log("after page");
  }

  private openAccontPage(){
    let modal = this.modalCtrl.create(AccountPage);
    modal.onDidDismiss(()=>{
      this.isRegistered = AppSettings.IS_REGISTERED || this.isLogin;
    });
    modal.present();  
  }


  ionViewDidEnter(){
     this.isRegistered = AppSettings.IS_REGISTERED;
     this.isLogin = AppSettings.IS_LOGIN;
  }
  
  private openLoginPage(){
    let modal = this.modalCtrl.create(LoginPage);
    modal.onDidDismiss(() => {
      this.isLogin = AppSettings.IS_LOGIN;
      this.isRegistered = AppSettings.IS_REGISTERED || this.isLogin;
    });
    modal.present();  
  }


}
