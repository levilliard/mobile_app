import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';

import { RegistrationPage } from '../registration/registration';
import { UserMessagePage } from '../user-message/user-message';
import { AboutPage } from '../about/about';
import { EventsPage } from '../events/events';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild(Content) content: Content;

  tab1Root:any = RegistrationPage;
  tab2Root:any = EventsPage;
  tab3Root:any = UserMessagePage;
  tab4Root:any = AboutPage;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
