import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AppSettings } from '../../providers/app-settings';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  language_ch: string;
  old_languange: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public viewCtrl: ViewController) {
  	this.language_ch = "";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  private changeLanguage(){
    switch (this.language_ch) {
      case "kr":
        this.translate.setDefaultLang(this.language_ch);
        AppSettings.LANGUAGE = this.language_ch;
        break;

      case "en":
        this.translate.setDefaultLang(this.language_ch);
        AppSettings.LANGUAGE = this.language_ch;
        break;

      case "fr":
        this.translate.setDefaultLang(this.language_ch);
        AppSettings.LANGUAGE = this.language_ch;
        break;

      default:
        this.translate.setDefaultLang("en");
        AppSettings.LANGUAGE = "en";
        break;
    }
  }

  changeState(){
    this.changeLanguage();
  }

  dismiss() {
   this.viewCtrl.dismiss();
  }

  validateChange(){
  	 this.changeLanguage();
  	 this.dismiss();
  }
}
