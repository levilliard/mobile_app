import { Component } from '@angular/core';
import { NavController, ModalController, MenuController } from 'ionic-angular';

import { SettingsPage } from '../settings/settings';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, private modalCtrl: ModalController, private menuCtrl: MenuController) {

  }

 openMenu() {
   this.menuCtrl.open();
 }

 goSettings(){
   let modal = this.modalCtrl.create(SettingsPage);
   modal.present();
 }
}
