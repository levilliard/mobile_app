import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { MapPage } from '../map/map';
import { Position } from '../../models/position';

@Component({
  selector: 'page-mupanah',
  templateUrl: 'mupanah.html',
})
export class MupanahPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MupanahPage');
  }

/*
  goMap(): void{
  	//, 
  	let destination = new Position(18.544166, -72.336917);
  	this.navCtrl.push(MapPage, {destination: destination});
  }
*/
}
