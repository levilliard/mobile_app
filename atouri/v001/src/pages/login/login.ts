import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Nav, ViewController, MenuController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { UserModel } from '../../models/user';
import { AppSettings } from '../../providers//app-settings';
import { GenericProvider } from '../../providers/generic'; 

import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  V
  private User:FormGroup;
  @ViewChild(Nav) nav: Nav;
  user: UserModel;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public formBuilder: FormBuilder, 
    public viewCtrl: ViewController,
    private gProvider: GenericProvider,
    private alertCtrl: AlertController,
    private storage: Storage
    ) {

    this.User = this.formBuilder.group({
      hUserUsername: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.required])],
      hUserPassword: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
    })
  }

  ionViewDidLoad() {

  }

  dismiss(): void{
     this.User.reset();
     this.viewCtrl.dismiss();
  }

  validateLogin(){
    let fake: UserModel ={
        "hUserId": "2",
        "hUserFirstname": "",
        "hUserLastname": "",
        "hUserTitle": "",
        "hUserSex": "M",
        "hUserPhone": "535345345",
        "hUserEmail": "fake@fake.com",
        "hUserUsername": "",
        "hUserPassword": "",
        "hUserCode": "509",
        "hUserImg": "",
        "hUserDetails": " .. ",
        "hUserDate": "2017-07-01"
    };

    fake.hUserUsername = this.User.value.hUserUsername.trim();
    fake.hUserPassword = this.User.value.hUserPassword;

    this.gProvider.login(fake, "/user/login").subscribe(data=>{
      this.user = <UserModel>data;
      if(this.user != null){
        AppSettings.IS_LOGIN = true;
        AppSettings.IS_REGISTERED = true;
        AppSettings.DEFAULT_USER = this.user;
        this.storage.set("USER", this.user);
        console.log("data: ", AppSettings.DEFAULT_USER);
        this.showAlert("Success");
      }else{
        this.showAlert("Login failed. Try to register");
      }
    }, error=>{
      console.log("login error", error);
      this.showAlert("Login failed. Try to register");
    });
     this.dismiss();
  }

  private showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'KasikRevo',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
}
