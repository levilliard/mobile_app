import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GenericProvider } from '../../providers/generic';

import { UserMessageModel } from '../../models/user-message';

/**
 * Generated class for the EventsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-events',
  templateUrl: 'events.html',
})
export class EventsPage {

  data: Array<UserMessageModel>;
  index = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams, private gProvider: GenericProvider){
  	this.data = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventsPage');
  }

  ionViewDidEnter(){
  	this.gProvider.getData("/user_message/all").subscribe(data=>{
  		console.log("data usr msg: ", data);
  		this.data = <Array<UserMessageModel>>data;
  	}, error=>{
  		console.log("user message error: ", error);
  	});  
  }

  getImgUser(uId){
  	let id:number = parseInt(uId);
  	let temp = id%3 + 1;
  	return "assets/img/user/" + temp + ".png";
  }

}
