import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content } from 'ionic-angular';
import { MessageModel } from '../../models/message';
import { GenericProvider } from '../../providers/generic';
import { UserMessageModel } from '../../models/user-message';
import { AppSettings } from '../../providers/app-settings';

import { LoginPage } from '../login/login';
import { SettingsPage } from '../settings/settings';

class Blogger {
	username: string;
	msg: string;
	img: string;
	
	constructor(username: string, msg: string, img: string) {
		// code...
		this.username = username;
		this.msg = msg;
		this.img = img;
	}
}

@Component({
  selector: 'page-user-message',
  templateUrl: 'user-message.html',
})
export class UserMessagePage {
  @ViewChild(Content) content: Content;

  blogger: Blogger;
  userTyping: string;
  messages: Array<Object>
  isLogin: boolean;
  currDate: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private gProvider: GenericProvider) {

  	this.messages = [];
  }

  ionViewDidLoad() {

  }

 addMsg(){
 	this.currDate = AppSettings.getCurrentDate(true);
 	this.isLogin = AppSettings.IS_LOGIN;

    if(this.isLogin){
	 	if(this.userTyping != null && this.userTyping.length > 1){

	 		//check tourist
	 		if(this.isInTourist(this.userTyping)){
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);
	 		 	
	 		 	setTimeout(()=>{
	 		 		let bg2 = new Blogger("Atouri", this.userTyping + " se yon zòn touris, ale nan meni an p'ou k vizite l'", "assets/img/user/bs.jpeg");
	 		    	this.messages.push(bg2);
	 		 	}, 1000);

	 		//msg important
	 		}else if(this.userTyping.trim().toLowerCase().indexOf("#atouri") != -1){
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);
	 		 	let bg2 = new Blogger("Atouri", "Mèsi paske ou kontakte nou, comantè ou a poste nan paj 'KONEKTE a'", "assets/img/user/bs.jpeg");

	 		    setTimeout(()=>{
	 		    	this.messages.push(bg2);
		 		}, 1000);

	 		    console.log("app set msg: ", AppSettings.DEFAULT_USER);
	 		    let userModel = new UserMessageModel("001", AppSettings.DEFAULT_USER.hUserId, AppSettings.DEFAULT_USER.hUserUsername, this.userTyping, AppSettings.getCurrentDate(true)+"");
	 		    this.gProvider.add(userModel, "/user_message").subscribe(data=>{
	 		    	console.log("Add important msg", data);
	 		    }, error=>{
	 		    	console.log("add important msg error: ", error);
	 		    }); 
	 		//help		
	 		}else if("helpèdaide".indexOf(this.userTyping.trim().toLowerCase()) != -1){
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);

	 		    setTimeout(()=>{
	 				let bg2 = new Blogger("Atouri", "Paj èd la. Epi ou ka chanje lang aplikasyon an", "assets/img/user/bs.jpeg");
	 		    	this.messages.push(bg2);	
	 		    	this.navCtrl.push(SettingsPage);	
		 		}, 1000);
		    //atouri
	 		}else if("atouri".indexOf(this.userTyping.trim().toLowerCase()) != -1){
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);
	 			let bg2 = new Blogger("Atouri", "Atouri, pou Ayiti Touris, pou ede moun vizit zòn touris ak zòn kilti Ayiti yo. Nou bay anpil detay sou chak zòn.", "assets/img/user/bs.jpeg");
	 		    this.messages.push(bg2);		
	 		}else if("manzè".indexOf(this.userTyping.trim().toLowerCase()) != -1 || "manzè".indexOf(this.userTyping.trim().toLowerCase()) != -1 || "kasika".indexOf(this.userTyping.trim().toLowerCase()) != -1 || "tizondife".indexOf(this.userTyping.trim().toLowerCase()) != -1){
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);

	 			let bg2 = new Blogger("Atouri", "TizonDife, Kaiska, nou se yon ekip, n'ap travay kifkif.", "assets/img/user/bs.jpeg");
	 		    this.messages.push(bg2);	

	 		 //contribute	
	 		}else if("potekole".indexOf(this.userTyping.trim().toLowerCase()) != -1){
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);

	 		    setTimeout(()=>{
	 				let bg2 = new Blogger("Atouri", "Mesi davans pou kontribisyon ou, n'ap voye ou sou sit nou an ", "assets/img/user/bs.jpeg");
	 		    	this.messages.push(bg2);	
	 		    	window.open("https://kasikrevo.herokuapp.com/");
		 		}, 1000);
		    //atouri
	 		}
	 		else{
	 			let bg1 = new Blogger(AppSettings.DEFAULT_USER.hUserUsername, this.userTyping,  "assets/img/user/user.png");
	 		    this.messages.push(bg1);
	 		 	let bg2 = new Blogger("Atouri", "M' pa konpran, tape youn nan mo sa yo pou: èd, help, aide, #atouri, atouri, potekole, ... Se kreyòl mw ka pale kounye a, mw ap aprann anglè ak fransè", "assets/img/user/bs.jpeg");
	 		    this.messages.push(bg2);	
	 		}
	 	}else{
	 		this.blogger = new Blogger("Atouri", "Ou pa ekri non !", "assets/img/user/bs.jpeg");
	 		this.messages.push(this.blogger);
	 	}
	 }else{
	 	this.blogger = new Blogger("Atouri", "Ou poko anrejistre, fe sa pou k ekri nou", "assets/img/user/bs.jpeg");
	 	this.messages.push(this.blogger);

	 	setTimeout(()=>{
	 		this.navCtrl.push(LoginPage);
	 	}, 1000);
	 }

 	this.content.scrollToBottom();
 	this.userTyping = "";
 }

 private parseWord(word: string): string{
 	return word.toLowerCase().replace("é", "e").replace("è", "e").replace("ê", "e").trim();
 }

 private isInTourist(criteria: string): boolean{
 	criteria = this.parseWord(criteria);
 	let list: string = "citadelsitadelbassinbasinbasenmakayasansousisansouci";
 	return list.indexOf(criteria) != -1;
 }

}
