import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TouristAreaModel } from '../../models/tourist-area';
import { MapPage } from '../map/map';
import { TouristDetailsPage } from '../tourist-details/tourist-details';
import { Position } from '../../models/position';

@Component({
  selector: 'page-details-tab',
  templateUrl: 'details-tab.html',
})
export class DetailsTabPage {

  tab1Root:any = TouristDetailsPage;
  tab2Root:any = MapPage;
  title = "Tourist Details"
  area: TouristAreaModel;
  destination: Position;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.area = this.navParams.get("area");
      if(this.area != null && this.area.touristAreaName != null){
        this.title = this.area.touristAreaName;
      }
      
      this.area.touristAreaLat = this.area.touristAreaLat.substring(0, 9);
      this.area.touristAreaLong = this.area.touristAreaLong.substring(0, 9);
      this.destination = new Position(parseFloat(this.area.touristAreaLat), parseFloat(this.area.touristAreaLong));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
