import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Geolocation } from '@ionic-native/geolocation';
import { TouristAreaModel } from '../../models/tourist-area';
import { AppSettings } from '../../providers/app-settings';
import { GenericProvider } from '../../providers/generic'

import { Position } from '../../models/position';

@Component({
  selector: 'page-tourist-details',
  templateUrl: 'tourist-details.html'
})

export class TouristDetailsPage {
  area: TouristAreaModel;
  img_uri = AppSettings.URL_BASE + "/file/download/tourist/";
  comments: Array<Object>;

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams, 
  	public menu: MenuController,
    private gProvider: GenericProvider,
    private geoLoc: Geolocation,
    private sSharing: SocialSharing) {
  		this.area = this.navParams.data.area;
  }

  ionViewDidLoad(){
    let position: Position = AppSettings.USER_GEOLOCATION;

    if(position == null){
       this.geoLoc.getCurrentPosition().then((position)=>{ 
          console.log("position check again: ", position);
          AppSettings.USER_GEOLOCATION = new Position(position.coords.latitude, position.coords.longitude);
        }, error=>{
          console.log("error geolocation", error);
        });
    }else{
          console.log("position check: ", position);
    }

    /*let url = "/message/user" + this.area.touristAreaId;
    this.gProvider.getData(url).subscribe(data=>{
      console.log("comments load...");
      this.comments = data;
    }, error=>{
      console.log("comments errors: ", error);
    })
    */
  }

  getImgUrl(data: string): string{
    if(data.indexOf("assets") != -1){
      return data + ".jpg";
    }

    return  AppSettings.URL_BASE + "/file/download" + AppSettings.URI_BASE +"/"+ data + ".jpg";    
  }

  likeMgr(val: number){
    if(this.area != undefined && this.area.touristAreaLike != undefined && this.area.touristAreaUnlike != undefined){
      if(val == 1){
        this.area.touristAreaLike++;
      }else if(val == -1){
        this.area.touristAreaUnlike++;
      }
      
      console.log("like mrg");

      this.gProvider.update(this.area, "/tourist").subscribe(data=>{
        console.log("tourist like add: ", data);
      }, error=>{
        console.log("tourist like add error: ", error);
      })
    }
  }


  public share(media: string){
    let url: string = "https://play.google.com/store/apps/details?id=ht.kasikrevo.atouri&hl=en";
    
    switch (media) {
      case "fb":
          this.sSharing.shareViaFacebook("KASIKREVO-Atouri (Ayiti Touris), " + this.area.touristAreaName + ".\n" + url, this.getImgUrl(this.area.touristAreaImg), null).then(()=>{
            console.log("Share via Facebook success: ", this.getImgUrl(this.area.touristAreaImg));
          }, error=>{
              console.log("Share via Facebook error", error);
          });
        break;
     
      case "tw":
          console.log("share via twitter");
          this.sSharing.shareViaTwitter("KASIKREVO-Atouri (Ayiti Touris), " + this.area.touristAreaName + ".\n" + url, this.getImgUrl(this.area.touristAreaImg), null).then(()=>{
            console.log("Share via twitter success");
          }, error=>{
              console.log("Share via twitter error", error);
          });
        break;

      case "wt":
          this.sSharing.shareViaWhatsApp("KASIKREVO-Atouri (Ayiti Touris), " + this.area.touristAreaName +  ".\n" + url, this.getImgUrl(this.area.touristAreaImg), null).then(()=>{
            console.log("Share via twitter WhatsApp: ", this.getImgUrl(this.area.touristAreaImg));
          }, error=>{
              console.log("Share via WhatsApp error", error);
          });;
        break;
      
      default:
        // code...
        break;
    }
  }

}
