import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, LoadingController } from 'ionic-angular';
import { GenericProvider } from '../../providers/generic';
import { TouristAreaModel } from '../../models/tourist-area';
import { AppSettings } from '../../providers/app-settings';
import { DetailsTabPage } from '../details-tab/details-tab';
import { MupanahPage } from '../mupanah/mupanah';

@Component({
  selector: 'page-culture-area',
  templateUrl: 'culture-area.html',
})
export class CultureAreaPage {
  @ViewChild(Content) content: Content;
  areas: Array<TouristAreaModel>;
  intvl = 0;
  other_areas: Array<TouristAreaModel>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    private gProvider: GenericProvider, 
    private loading: LoadingController) {
    this.areas = [];
    this.other_areas = [];
    this.init();

    AppSettings.URI_BASE = "/culture";
    this.gProvider.getData("/tourist/all/culture").subscribe(data=>{
        let temp = <Array<TouristAreaModel>>data;
        let i = temp.length;
        let j = this.areas.length;

        while(i--){
          this.areas[j+i-1] = temp[i];
        }
        
          this.other_areas = this.areas;
      }, error=>{

      });

    this.presentLoading(" ");
  }

  presentLoading(param){
    let load = this.loading.create({content: param});

    load.present();

    setTimeout(()=>{
      load.dismiss();
    }, 2000);
  }

  ionViewDidLoad(){
        console.log("data culture: ", this.other_areas.length);
  }

  getIMGUri(data): string{
    if(data.indexOf("assets") != -1){
      return data + ".jpg";
    }

    return  AppSettings.URL_BASE + "/file/download"+ AppSettings.URI_BASE + "/" + data + ".jpg";    
  }


  openPageAreaDetails(area: TouristAreaModel) {
    this.navCtrl.push(DetailsTabPage, {area: area});
  }

  getItems(ev: any) {
    let val = ev.target.value;

    let list = [];
    if (val && val.trim() != '') {
      for(let i = 0; i < this.areas.length; ++i){
         let str = this.areas[i].touristAreaName.trim();
         if(str.trim().toLowerCase().indexOf(val.toLowerCase()) > -1){
            list.push(this.areas[i]);
         }
       }
      }else{
        this.other_areas = this.areas;
      }

      if(this.other_areas.length != list.length && list.length > 0){
        this.other_areas = list;
      }

      if(list.length == 0){
        this.other_areas = this.areas;
      }
   }

   moveUp(): void{
     this.content.scrollToTop();
   }

   goMupanah(): void{
     this.navCtrl.push(MupanahPage);
   }
   private init(): void{

   let ofl: Array<TouristAreaModel> = [{ 
        "touristAreaId": "13",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "FOKAL",
        "touristAreaDesc": "FOKAL-Fondation Konesans ak Libète. La Fondation naît en avril 1995 après mandat octroyé à Michèle Duvivier Pierre-Louis par Karen J. Greenberg, vice-présidente des programmes du Open Society Institute créé dans les années 80 par le philanthrope Georges Soros. Ce réseau de fondations, projets et institutions partenaires, établi dans plus d’une centaine de pays est connu aujourd’hui sous le nom de Open Society Foundations (OSF).",
        "touristAreaImg": "assets/img/data/fokal",
        "touristAreaDetails": "La Fondation Connaissance et Liberté, créée en 1995 et reconnue d’utilité publique depuis 2000, est une fondation nationale haïtienne financée principalement par l’Open Society Foundations. L’Open Society Foundations est un réseau de fondations et d’initiatives établies à travers le monde par le financier hongrois américain George Soros pour la promotion des valeurs démocratiques. La FOKAL reçoit aussi des financements de l'Union Européenne et de la coopération Française.",
        "touristAreaLoc": "Port-au-Prince",
        "touristAreaLat": "18.533622",
        "touristAreaLong": "-72.336098",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "centre culturel",
        "touristAreaCat": "tourist",   
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    },
    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "Centre culturel Katherine Dunham",
        "touristAreaDesc": "Le Centre Culturel Katherine Dunham fait partie intégrante du parc de Martissant. Ce parc est un espace public qui se veut non seulement un parc botanique et médicinal, mais aussi un espace culturel, éducatif et récréatif. Le parc est formé par un ensemble de quatre anciennes propriétés privées (l’habitation Leclerc, les Résidences Dunham et Mangones et l’habitation Pauline) devenues publiques par l’arrêté présidentiel de 2007 qui a créé le parc de Martissant. L’ensemble fait environ 17 hectares.",
        "touristAreaImg": "assets/img/data/cckd",
        "touristAreaDetails": "",
        "touristAreaLoc": "Martissant 23, Port-au-Prince",
        "touristAreaLat": "18.527827",
        "touristAreaLong": "-72.361626",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "centre culturel",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    }
    ,
    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "BNE-Bureau National d'Ethnologie",
        "touristAreaDesc": "Le Bureau National d’Ethnologie est une institution de l’Etat haïtien fondée par Jacques Roumain le 31 Octobre 1941. En 1984, sous la présidence à vie de Son Excellence Jean Claude Duvalier, l’organisme a changé de dénomination pour devenir le Bureau National d’Ethnologie.",
        "touristAreaImg": "assets/img/data/bne",
        "touristAreaDetails": "",
        "touristAreaLoc": "Port-au-Prince",
        "touristAreaLat": "18.542269",
        "touristAreaLong": "-72.338185",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "lieu culturel",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    }
        ,
    { 
        "touristAreaId": "10",
        "communeId": "1",
        "sectionCommId": "101",
        "touristAreaName": "ENARTS",
        "touristAreaDesc": "ENARTS-Ecole Nationale des Arts (UEH).\nArts Plastiques, Danse, Théâtre, Musique, Histoire de l'art, Gestion culturelle.",
        "touristAreaImg": "assets/img/data/enarts",
        "touristAreaDetails": "",
        "touristAreaLoc": "Port-au-Prince",
        "touristAreaLat": "18.541412",
        "touristAreaLong": "-72.342845",
        "touristAreaCreatedBy": "levilliard",
        "touristAreaDateCreated": "2017-07-14 12:00:00",
        "touristAreaModifyBy": "levilliard",
        "touristAreaDateModify": "2017-07-14 12:00:00",
        "touristAreaType": "lieu culturel",
        "touristAreaCat": "tourist",        
        "touristAreaLike": 0,
        "touristAreaUnlike": 0
    }];


//18.541412, 
    let i = ofl.length;
    while(i--){
      this.areas.push(ofl[i]);
    }

    this.other_areas = this.areas;
   }
}