
import { UserModel } from '../models/user';
import { Position } from '../models/position';

export class AppSettings {
	public static URL_BASE = "https://atouri.herokuapp.com/atouri";
  public static DATA = [];
	public static LANGUAGE = "ENG";
	public static CURENCY_CHANGE = "HTG";
	public static IS_LOGIN = false;
  public static IS_REGISTERED = false;
  public static SHOW_HEAD = true;
	public static DEFAULT_USER = new UserModel("1", "Atouri", "Atouri", "Atouri", "F", "0000", "ragagann@duckduck.com", "Manzè", "", "", "assets/img/user/user.jpg", "", "");
  public static USER_GEOLOCATION: Position;
  public static IS_ONLINE = false;
  public static URI_BASE: string = "/tourist";

  public static  getCurrentDate(timezone: boolean): string{
    let dt = new Date();
    if(timezone){
        return dt.getFullYear() + "-" + ("0" + (dt.getMonth() + 1)).slice(-2) + "-" + ("0" + (dt.getDay() + 1)).slice(-2) + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds(); 
      }else{
      	return dt.getFullYear() + "-" + ("0" + (dt.getMonth() + 1)).slice(-2) + "-" + ("0" + (dt.getDay() + 1)).slice(-2); 
    }
  }
}
