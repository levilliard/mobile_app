
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Storage } from '@ionic/storage';

import { AppSettings } from './app-settings';


@Injectable()
export class GenericProvider {

  constructor(public http: Http, private storage: Storage){

  }
   
  public initStorage(data_key: string, uri: string):void{
  	   this.getData(uri).subscribe(data=>{
	      this.storage.ready().then(() => {
	       	  this.storage.set(data_key, data);
	      });
  	   }, error=>{
  	   		console.log("error: ", error);
  	   });
   }

  // web api service
  public getData(uri: string): Observable<Object[]>{
    let url = AppSettings.URL_BASE + uri;
    return this.http.get(`${url}`)
         .map(res => <Object[]>res.json());
  }    


  public update(body: Object, uri: string): Observable<Response>{
 		let bodyStr = JSON.stringify(body);
 		let headers = new Headers({'Content-Type': 'application/json'});
    console.log("update data..");
 		let url = AppSettings.URL_BASE + uri;
 		return this.http.put(
 			url,
 			bodyStr,
 			{headers: headers}
 		);
 	}

  public add(body: Object, uri: string): Observable<string>{
 		let bodyStr = JSON.stringify(body);
 		let headers = new Headers({'Content-Type': 'application/json'});

 		let url = AppSettings.URL_BASE + uri;
 		return this.http.post(
 			url,
 			bodyStr,
 			{headers: headers}
 		).map(res=> <Object>res.toString());
 	}


  public login(body: Object, uri: string): Observable<Object>{
     let bodyStr = JSON.stringify(body);
     let headers = new Headers({'Content-Type': 'application/json'});

     let url = AppSettings.URL_BASE + uri;
     return this.http.post(
       url,
       bodyStr,
       {headers: headers}
     ).map(res=> <Object>res.json());
   }


  public deleteObj(id: string, uri): Observable<Response>{
 		let url = AppSettings.URL_BASE + uri;
 		return this.http.delete(`${url}`);
 	}
}

