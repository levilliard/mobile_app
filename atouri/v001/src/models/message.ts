
export class MessageModel{
	messageId: string;
    hUserId: string;
    touristAreaId: string;
    messageContent: string;
    messageDate: string;

    constructor(id: string, sId: string, rId: string, content: string, mDate: string){
    	this.messageId = id;
    	this.hUserId = sId;
    	this.touristAreaId = rId;
    	this.messageContent = content;
    	this.messageDate = mDate;
    }
}