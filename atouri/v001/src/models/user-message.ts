
export class UserMessageModel{
	userMessageId: string;
	hUserId: string;
	hUserUsername: string;
	userMessageContent: string;
	userMessageDate: string;

	constructor(mId: string, hId: string, uname: string, content: string, time: string){
		this.userMessageId = mId;
		this.hUserId = hId;
		this.hUserUsername = uname;
		this.userMessageContent = content;
		this.userMessageDate = time;
	}
}