
/*
	App: Atouri
	Author: TizonDife
	Date: April 2017
*/

export class TouristAreaModel{
	touristAreaId: string;
	communeId: string;
	sectionCommId: string;
	touristAreaName: string;
	touristAreaDesc: string;
	touristAreaImg: string;
	touristAreaDetails: string;
	touristAreaLoc: string;
	touristAreaLat: string;
	touristAreaLong: string;
	touristAreaCreatedBy: string;
	touristAreaDateCreated: string;
	touristAreaModifyBy: string;
	touristAreaDateModify: string;
	touristAreaType: string;
	touristAreaCat: string;
	touristAreaLike: number;
	touristAreaUnlike: number;

	constructor(Id: string, Com: string, Sec: string, Name: string, Desc: string, Img: string, Details: string, Loc: string, Lat: string, Long: string, cBy: string, dCreated: string, mBy: string, dModify: string, cat: string, type: string, Like: number, Unlike: number) {
		this.touristAreaId = Id;
		this.communeId = Com;
		this.sectionCommId = Sec;
		this.touristAreaName = Name;
		this.touristAreaDesc = Desc;
		this.touristAreaImg = Img;
		this.touristAreaDetails = Details;
		this.touristAreaLoc = Loc;
		this.touristAreaLat = Lat;
		this.touristAreaLong = Long;
		this.touristAreaCreatedBy = cBy;
		this.touristAreaDateCreated = dCreated;
		this.touristAreaModifyBy = mBy;
		this.touristAreaDateModify = dModify;
		this.touristAreaCat = cat;
		this.touristAreaType = type;
		this.touristAreaLike = Like;
		this.touristAreaUnlike = Unlike;
	}
}
