import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Http } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { Network } from '@ionic-native/network';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { SocialSharing } from '@ionic-native/social-sharing';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TouristAreaPage } from '../pages/tourist-area/tourist-area';
import { TouristDetailsPage } from '../pages/tourist-details/tourist-details';
import { CultureAreaPage } from '../pages/culture-area/culture-area';
import { MapPage } from '../pages//map/map';
import { EventsPage } from '../pages/events/events';
import { UserMessagePage } from '../pages/user-message/user-message';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { RegistrationPage } from '../pages/registration/registration';
import { TabsPage } from '../pages/tabs/tabs';
import { AboutPage } from '../pages/about/about';
import { SettingsPage } from '../pages/settings/settings';
import { DetailsTabPage } from '../pages/details-tab/details-tab';
import { MupanahPage } from '../pages/mupanah/mupanah';
import { GenericProvider } from '../providers/generic';

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TouristAreaPage,
    TouristDetailsPage,
    CultureAreaPage,
    EventsPage,
    UserMessagePage,
    MapPage,
    RegistrationPage,
    AccountPage,
    LoginPage,
    TabsPage,
    AboutPage,
    SettingsPage,
    DetailsTabPage,
    MupanahPage
    ],
  imports:[
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {tabsHideOnSubPages:"true", backButtonText: '', pageTransition: 'ios-transition'}),
    IonicStorageModule.forRoot(),
    IonicImageViewerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }}),
    IonicStorageModule.forRoot(),
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TouristAreaPage,
    TouristDetailsPage,
    MapPage,
    CultureAreaPage,
    UserMessagePage,
    EventsPage,
    RegistrationPage,
    LoginPage,
    AccountPage,
    TabsPage,
    AboutPage,
    SettingsPage,
    DetailsTabPage,
    MupanahPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GenericProvider,
    Geolocation,
    SocialSharing,
    Network, 
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
