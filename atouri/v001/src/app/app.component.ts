import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { TouristAreaPage } from '../pages/tourist-area/tourist-area';
import { CultureAreaPage } from '../pages/culture-area/culture-area';
import { TabsPage } from '../pages/tabs/tabs';
import { UserModel } from '../models/user';
import { Position } from '../models/position';

import { AppSettings } from '../providers/app-settings';
import { GenericProvider } from '../providers/generic';

import { Geolocation } from '@ionic-native/geolocation';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  pages: Array<{title: string, component: any}>;

  constructor(
    private platform: Platform, 
    statusBar: StatusBar, 
    public menu: MenuController, 
    public translate: TranslateService,
    splashScreen: SplashScreen, 
    private geoLoc: Geolocation,
    private storage: Storage,
    private alertCtrl: AlertController,
    private gProvider: GenericProvider) {
      
      //menu
      this.pages = [
      {title: 'Home', component: HomePage},
      {title: 'Tourist', component: TouristAreaPage},
      {title: 'Culture', component: CultureAreaPage},
      {title: 'Divers', component: TabsPage},
      ];
      
      //lang
      this.translate.setDefaultLang('fr');

      //user auth
      this.storage.get("USER").then(user=>{
          if(user != null){
            AppSettings.DEFAULT_USER = <UserModel>user;
            AppSettings.IS_LOGIN = true;
            AppSettings.IS_REGISTERED = true;
            //login
            this.gProvider.login(AppSettings.DEFAULT_USER, "/user/login").subscribe(data=>{
                console.log("data: ", data);
                let user: UserModel = <UserModel>data;
                  AppSettings.IS_LOGIN = true;
                  AppSettings.DEFAULT_USER = user;
                  this.storage.set("USER", user);
              }, error=>{
                console.log("login error", error);
              });

          }
      }, error=>{
         console.log("not found: ", error);
      });



      platform.ready().then(() =>{
        this.geoLoc.getCurrentPosition().then((position)=>{ 
          console.log("position: ", position);
          AppSettings.IS_ONLINE = true;
          AppSettings.USER_GEOLOCATION = new Position(position.coords.latitude, position.coords.longitude);
        }, error=>{
          console.log("error geolocation", error);
        });

        statusBar.styleDefault();
        splashScreen.hide();
    });
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page);
  }

/*
  private showAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'KasikRevo',
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  */
  goRegister(){

  }
}

